# Firefox Ubuntu-Frame Kiosk

## Establish SSH connection

### Host (Kiosk)

* Install Ubuntu Desktop
* Connect to network
* Log-in to Ubuntu SSO
* Update
  `sudo apt update && sudo apt upgrade && sudo apt autoremove`
* Install dependencies  
  `sudo apt install openssh-server ufw git
* Enable firewall  
  `sudo ufw allow ssh && sudo ufw enable`  
* Get IP for ssh connection  
  `ip a`

### Client (dev)

* `ssh <user>@<ipaddr>`
* [Configure automatic login](https://help.gnome.org/admin/system-admin-guide/stable/login-automatic.html.en)
* Clone this repo  
  `sudo mkdir /opt/firefox-kiosk && sudo git clone https://gitlab.com/raQai/firefox-kiosk.git /opt/firefox-kiosk`
* Start installer [install.sh](./install.sh)  
  `sudo /opt/firefox-kiosk/install.sh`
* Reboot and verify everything is working

