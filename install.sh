#!/bin/bash

function install()
{
  export WAYLAND_DISPLAY=wayland-0
  export MOZ_ENABLE_WAYLAND=1

  local SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

  function setup_dependency()
  {
    local src="$1"
    local dst="$2"

    # remove sym link before creating a new one instead of using -f to
    # ensure linking folders will not result in nested links
    [ -L "${dst}" ] && rm "${dst}"

    ln -s "${src}" "${dst}"
  }

  function setup_firefox()
  {
    snap remove firefox
    sudo add-apt-repository ppa:mozillateam/ppa
    sudo apt update
    sudo apt install firefox-esr
  }

  function setup_ubuntu_frame()
  {
    snap install ubuntu-frame
    snap set ubuntu-frame config="cursor=null"
    snap set ubuntu-frame daemon=true

    snap install ubuntu-frame-osk
    snap connect ubuntu-frame-osk:wayland
    snap set ubuntu-frame-osk layout=de
    snap set ubuntu-frame-osk daemon=true
  }

  function setup_kiosk()
  {
    setup_dependency \
      "${SCRIPTPATH}/kiosk.service" \
      "/etc/systemd/system/kiosk.service"

    setup_dependency \
      "${SCRIPTPATH}/kiosk.timer" \
      "/etc/systemd/system/kiosk.timer"

    systemctl daemon-reload
    systemctl enable --now kiosk.timer
  }

  disable_screen_lock
  setup_firefox
  setup_ubuntu_frame
  setup_kiosk
}

install "$@"
