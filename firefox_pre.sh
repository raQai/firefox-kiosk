#!/bin/bash

function run()
{
  local src_path="/opt/firefox-kiosk"
  local mozilla_path="${HOME}/.mozilla"

  local profile_name="default"
  local profile="profile.${profile_name}"
  local profile_dst="${mozilla_path}/firefox-esr/${profile}"

  local policies_name="policies.json"
  local policies_src="${src_path}/${policies_name}"
  local policies_dst="/etc/firefox/policies/${policies_name}"

  local styles_name="chrome"
  local styles_src="${src_path}/${styles_name}"
  local styles_dst="${profile_dst}/${styles_name}"

  function setup_dependency()
  {
    local src="$1"
    local dst="$2"

    # remove sym link before creating a new one instead of using -f to
    # ensure linking folders will not result in nested links
    [ -L "${dst}" ] && rm "${dst}"

    ln -s "${src}" "${dst}"

    return 0
  }

  function create_default_profile()
  {
    [ ! -d "${profile_dst}" ] \
      && firefox-esr -CreateProfile "${profile_name} ${profile_dst}"

    return 0
  }

  rm -rf "${mozilla_path}" \
    && create_default_profile \
    && setup_dependency "${policies_src}" "${policies_dst}" \
    && setup_dependency "${styles_src}" "${styles_dst}"

  return 0
}

run
